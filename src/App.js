import { useEffect} from "react";
import { Route, Routes } from "react-router-dom";
import Header from './components/Header'
import style from './main.scss'
import { Basket } from "./pages/Basket";
import { Favorite } from "./pages/Favorite";
import { Home } from "./pages/Home";
import { useDispatch,useSelector} from "react-redux";
import {getData} from './redux/actions/Product'
export default function App(){
  const {favorite,basket} = useSelector(state=>state.data)
  const dispatch =useDispatch()
  useEffect(()=>{
 dispatch(getData())
},[dispatch]) 

useEffect(()=>{
      localStorage.setItem('basket',JSON.stringify(basket))
      localStorage.setItem('favorite',JSON.stringify(favorite))
},[favorite,basket])
return (<>
      <Routes>
      <Route 
            path="/" element={<Header/>}>
        <Route 
              index
              element={<Home/>}/>
        <Route 
              path="/basket" 
              element={<Basket/>}/>
        <Route 
              path="/favorite" 
              element={<Favorite/>}/>
        </Route>
      </Routes>  
      </>
    )
   } 


