
import List from "../List";
import styles from './index.module.scss'
import Modal from '../Modal'
import { useSelector } from "react-redux";

export default function Card (props){
  const { modal,id } = useSelector((state)=>state.window)
  const {addToBasket,description,color,text,dell,data}= props
  return( 
            <div className={styles.cardblock}>
            {data?.map((item,num) =>(
            <List 
              key={num}
              dell={dell}
              data={data}
              text={text}
              fill={color}
              addToBasket={addToBasket}
            {...item}/>
            )
            )}
            {modal&&<Modal 
                id="modal" 
                description={description}
                title="Підтвердження" 
                backgroundColor="rgba(37, 40, 40)" 
                data={id}
                addToBasket ={addToBasket}/>}
            </div>
        )
    }
