import style from './index.module.scss'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { useDispatch} from 'react-redux'
import { closeForma} from '../../redux/actions/Forma'
import { buyFromBasket } from '../../redux/actions/Product'
export function FormaOfBuying(){
    const dispatch =useDispatch()
    const forma = useFormik({
        initialValues:{
            firstName:'',
            lastName:'',
            age:'',
            address:'',
            phone:''
        },
        validationSchema:Yup.object({
            firstName:Yup.string().max(15, "Ім'я складається з максимум 15 символів").required("Поле обов'язкове"),
            lastName:Yup.string().max(15, "Прізвище складається з максимум 15 символів").required("Поле обов'язкове"),
            age:Yup.number().moreThan(17,"Необхідно ,щоб було більше 18 років").required("Поле обов'язкове"),
            address:Yup.string().max(20, "Ім'я складається з максимум 20 символів").required("Поле обов'язкове"),
            phone:Yup.string().length(12,"Введіть номер у форматі '38xxxxxxxxx' ,без пробілів і без додаткових знаків").required("Поле обов'язкове")
        }),
        onSubmit:((value)=>{
            console.log('Покупець',value)
            console.log('Товари',JSON.parse(localStorage.getItem("basket")))
            dispatch(buyFromBasket())
            dispatch(closeForma())
            

        })
    })
  
    return(
        <section className={style.wrapper}>
            <form onSubmit={forma.handleSubmit} className={style.forma}>
                <div 
                className={style.closeForm}
                onClick={()=>(dispatch(closeForma()))}>&times;</div>
                <div className={style.inputWrapper}>
                <label htmlFor="firstName">Ім'я користувача</label>
                    <input 
                        onChange={forma.handleChange}
                        onBlur={forma.handleBlur}
                        type="text" 
                        name="firstName"
                        value={forma.values.firstName} 
                        placeholder="Введіть Ім'я" 
                        className=""/>
                {forma.touched.firstName&& forma.errors.firstName?<p className={style.err}>{forma.errors.firstName}</p>:""}
                <label htmlFor="lastName">Прізвище користувача</label>
                    <input 
                        onChange={forma.handleChange}
                        onBlur={forma.handleBlur}
                        type="text" 
                        name="lastName"
                        value={forma.values.lastName} 
                        placeholder="Введіть Прізвище" 
                        className=""/>
                {forma.touched.lastName&& forma.errors.lastName?<p className={style.err}>{forma.errors.lastName}</p>:""}

                <label htmlFor="age">Вік</label>
                    <input
                        onChange={forma.handleChange}
                        onBlur={forma.handleBlur}
                        type="text" 
                        name="age" 
                        value={forma.values.age} 
                        placeholder="Введіть Вік" 
                        className=""/>
                {forma.touched.age && forma.errors.age?<p className={style.err}>{isNaN(forma.values.age)?'Введіть будь-ласка число':forma.errors.age }</p>:""}

                <label htmlFor="address">Адреса доставки</label>
                    <input
                        onChange={forma.handleChange} 
                        onBlur={forma.handleBlur}
                        type="text" 
                        name="address"
                        value={forma.values.address} 
                        placeholder="Введіть Адресу доставки" 
                        className=""/>
                {forma.touched.address && forma.errors.address?<p className={style.err}>{forma.errors.address}</p>:""}

                <label htmlFor="phone">Мобільний телефон</label>
                    <input 
                        onChange={forma.handleChange}
                        onBlur={forma.handleBlur}
                        type="text" 
                        name="phone"
                        value={forma.values.phone} 
                        placeholder="Введіть Мобільний телефон" 
                        className=""/>
                {forma.touched.phone&& forma.errors.phone?<p className={style.err}>{!Number(forma.values.phone)?'Введіть будь-ласка число': forma.errors.phone}</p>:""}
                </div>
            <button className={style.btnBuy} type="submit">Купити товар</button>
        </form>
    </section>
    )
}