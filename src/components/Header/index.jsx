import styles from './index.module.scss'
import { useSelector } from 'react-redux';
import { Link, Outlet } from 'react-router-dom';
export default function Header() {
    const {favorite,basket} = useSelector(state=>state.data)
        return(
            <>
            <div className={styles.navbar}>
                <Link to="/">
                    <img className={styles.log} src='./image/rozetka.png'alt='main logo' />
                </Link>
                     <p className={styles.slogan}>We make your life more better and comfortable</p>
                <Link to="/basket">
                    <img className={styles.icon} src='./image/icon.png' alt='in basket'/>
                </Link>
                <Link to="/favorite">
                    <img className={styles.iconstar} src='./image/star.png' alt='favorite'/>
        
                </Link>
                     <p 
                    className={styles.goods}>{basket.length}
                    </p>
                    <p 
                    className={styles.star}>{favorite.length}</p>
            </div>
            <Outlet/>
            </>
        )
    }
