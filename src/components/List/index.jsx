import styles from './index.module.scss'
import SVG from "../SVG";
import SVGRed from "../SVGRed";
import { useState } from "react";
import { useDispatch,useSelector } from 'react-redux';
import { openModal } from '../../redux/actions/Modal';
import { deleted } from '../../redux/actions/Product'
export default function List(props){
    const [IsFavorite, setIsFavorite]= useState(true)
    const dispatch = useDispatch()
    const {favorite} =useSelector(state=>state.data)
    const {name,article,dell,picture,price,text,color} =props
    const changeStatus =()=> {
        setIsFavorite((prev)=>!prev)
    }
        return(
            <div 
                className={styles.card}>
            <div 
                className={styles.squer} 
                style={{background:color}}></div>
            <h2>{name}</h2>
            <div className={styles.deleteBtn}
                data-id={article} 
                onClick={()=>
                (dispatch(openModal(article)))}>{dell}</div>
            <img 
                className={styles.img} 
                src={picture} 
                alt={name}/>
            <p className={styles.price}>{price}</p>
            <button 
                data-id={article}
                className={styles.btn} 
                onClick={()=>dispatch(openModal(article))}
            >{text}</button>
            {favorite?.find(key=> key.article === article)?
                <SVGRed
                    stateOfStar={IsFavorite}
                    id={article}
                    deleted={(e)=>dispatch(deleted(e,favorite))}
                    onClick={changeStatus}
                    className={styles.svg} 
                    />:
                <SVG 
                    changeStatus={changeStatus}
                    id={article}
                    className={styles.svg} 
                    width="30" 
                    height="30"
                />  
           }        
            </div> 
        )
        }
