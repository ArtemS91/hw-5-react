import { useDispatch} from 'react-redux'
import styles from './style.module.scss'
import { closeModal } from '../../redux/actions/Modal'
export default function Modal(props) {
    const dispatch =useDispatch()
    const {id,backgroundColor,data,description,addToBasket} =props
        return(
            <section 
                className={styles.modal} 
                id={id}
                onClick={(e)=>e.target.hasAttribute('id')?(dispatch(closeModal())):''}>
                <div 
                    className={styles.modal__content} 
                    style={{backgroundColor:backgroundColor}}>
                    <div className={styles.modal__content__close} 
                        id={id}
                        data-id={data}
                        onClick={(e)=>(
                        dispatch(closeModal())
                    )}>&times;</div>
                        <h1 className={styles.modal__content__title}>{props.title}</h1>
                        <p >{description}</p>
                    <div>
                        <button 
                            className={styles.modal__content__btn}
                            id={id} 
                            data-id={data}
                            onClick={addToBasket}>Так</button>
                        <button 
                            className={styles.modal__content__btn} 
                            onClick={()=>dispatch(closeModal())} >Ні</button>
                    </div>
                </div>
            </section>
        )
    }
