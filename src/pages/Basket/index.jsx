import Card from "../../components/Card";
import style from './index.module.scss'
import { FormaOfBuying } from '../../components/Forma'
import { useDispatch,useSelector} from "react-redux";
import { deleteItemFromBasket} from "../../redux/actions/Product"; 
import { openForma } from '../../redux/actions/Forma'
export function Basket(){
    const dispatch = useDispatch();
    const {forma} = useSelector(state=>state.confirmation)
    const {basket,favorite} =useSelector(state=>state.data)
    return(<>
    {basket.length !==0?<Card 
              data={basket}
              favorite={favorite}
              text="Видалити з корзини"
              description="Ви хочете видалити цей товар з кошику?"
              dell='&times;'
              addToBasket={(e)=>(dispatch(deleteItemFromBasket(e,basket)))}
              />:<div className="info">У кошику нічого немає</div>}
              <button 
              className={style.btnBuy}
              onClick={()=>dispatch(openForma())}
              >Оформити замовлення</button>
            {forma&&<FormaOfBuying/>} 
    </>)
}
