import Card from "../../components/Card";
import { useDispatch,useSelector} from "react-redux";
import { addToBasket } from "../../redux/actions/Product";
export function Favorite(){
    const dispatch = useDispatch();
    const {favorite,array,basket} =useSelector(state=>state.data)

    return (<>
        {favorite.length !==0?<Card 
                data={favorite}
                favorite={favorite}
                text="Додати до корзини"
                description="Ви хочете додати цей товар у кошик?"
                addToBasket={(e)=>(dispatch(addToBasket(e,array,basket)))}
              />:<div className="info">Нічого немає в улюбленому</div>}
    </>)
}
