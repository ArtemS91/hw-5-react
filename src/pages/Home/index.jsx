import Card from "../../components/Card";
import { useDispatch,useSelector } from "react-redux";
import { addToBasket } from "../../redux/actions/Product";

export function Home(){
    const dispatch = useDispatch();
    const {array,favorite,basket} =useSelector(state=>state.data)
    return(
        <>
        <Card
        data={array}
        favorite={favorite}
        addToBasket={(e)=>(dispatch(addToBasket(e,array,basket)))} 
        text="Додати до корзини"
        description="Ви хочете додати цей товар у кошик?"
        />
        </>
    )
}
