import {formaType} from "../types"

export function openForma() {
    return {
        type: formaType.SHOW_FORM,
    }
}

export function closeForma(){
    return {
        type:formaType.CLOSE_FORM
    }
}

