
import {loginTypes} from "../types";

export function openModal(id) {
    return {
        type: loginTypes.SHOW_MODAL,
        payload:{id}
    }
}

export function closeModal(){
    return {
        type:loginTypes.CLOSE_MODAL
    }
}