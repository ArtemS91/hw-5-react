import {data,formaType} from '../types'
import {parseLocalStorageItem} from '../../helper/index'

export function dataValue(product){
    return{
        type:data.GET_DATA,
        payload:{
            array:product.array,
            basket:parseLocalStorageItem('basket'),
            favorite:parseLocalStorageItem('favorite')
        }
        
    }
}
export function addToBasket(e,array){
    let id= e.target.dataset.id  
    const basketArray =array.find(elem=> elem.article === id)
    return {
        type:data.ADD_TO_BASKET,
        payload:[basketArray]
    }
}
export function deleteItemFromBasket(e,array){
    let id= e.target.dataset.id
    const deleteFromBasketElementInd =array.findIndex(fav=>fav.article === id)
    const deleteFromBasketArray=[
        ...array.slice(0,deleteFromBasketElementInd),
        ...array.slice(deleteFromBasketElementInd + 1)    
    ]
    return {
        type:data.DELETE_FROM_BASKET,
        payload:deleteFromBasketArray
    }
}
export function addToFavorites(e,array){
    let id= e.target.id
    const favoriteArray =array.find(elem=> elem.article === id)
    return {
        type:data.ADD_TO_FAVORITE,
        payload:[favoriteArray]
    }
}
export function deleted(e,array){
  let id= e.target.dataset.id;
  const deleteFromFavorite =array.filter(fav=>fav.article !== id) 
    return {
        type:data.DELETE_FROM_FAVORITE,
        payload:deleteFromFavorite
    }
} 
export function buyFromBasket(){
    
    return {
        type:formaType.BUY_FROM_BASKET,
        payload:[]
    }
}

export function getData(){
    return async function(dispatch){
        const product =await fetch("../mock/data.json").then(res=>res.json())
        const newDate ={
            array:product,
            favorite:parseLocalStorageItem('favorite'),
            basket:parseLocalStorageItem('basket')
        }
        dispatch(dataValue(newDate))

    };
   
}