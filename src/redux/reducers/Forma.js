import {formaType} from "../types"
const initialState={
    forma:false
}
export function formConfirmation(state = initialState, action) {
    switch (action.type) {
        case formaType.SHOW_FORM:
            return {
                forma: true
            }
        case formaType.CLOSE_FORM:
            return {
                forma: false
            }
        
        default:
            return state;
    }
}