import {loginTypes} from "../types.js";

const initialState={
    modal:false,
    id:0
}

export function modalWindow(state = initialState, action) {
    switch (action.type) {
        case loginTypes.SHOW_MODAL:
            return {
                modal: true,
                id:action.payload.id
            }
        case loginTypes.CLOSE_MODAL:
            return {
                modal: false
            }
        default:
            return state;
    }
}
