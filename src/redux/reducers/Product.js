import {data,formaType} from '../types'
import { parseLocalStorageItem } from '../../helper/index'
const initialState = {
    array:[],
    favorite:parseLocalStorageItem('favorite'),
    basket:parseLocalStorageItem('basket'),
};

export function dataReducer(state=initialState,action){
switch(action.type){
    case data.GET_DATA:
        return action.payload
    case data.ADD_TO_FAVORITE:
        return {...state,favorite:[...state.favorite,...action.payload]}
    case data.DELETE_FROM_FAVORITE:
        return {...state,favorite:action.payload}
    case data.ADD_TO_BASKET:
        return {...state,basket:[...state.basket,...action.payload]}
    case data.DELETE_FROM_BASKET:
        return {...state,basket:action.payload}
    case formaType.BUY_FROM_BASKET:
            return {...state,basket:[]}
    default :
        return state
}
}