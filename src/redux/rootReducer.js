// Core
import { combineReducers } from "redux";
// Reducers
import { modalWindow as window } from "./reducers/Modal";
import { dataReducer as data } from './reducers/Product'
import { formConfirmation as confirmation } from './reducers/Forma'

export const rootReducer = combineReducers({window,data,confirmation});

